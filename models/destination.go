package models

type Destination struct {
	ID          string `gorm:"primary_key" json:"id"`
	CountryID   string `json:"country_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Image       string `json:"image"`
	Prop        string `json:"prop"`
}
