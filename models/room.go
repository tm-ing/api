package models

type Room struct {
	ID              string  `gorm:"primary_key" json:"id"`
	HotelID         string  `json:"hotel_id"`
	Name            string  `json:"name"`
	Description     string  `json:"description"`
	LowSeasonPrice  float32 `json:"low_season_price"`
	HighSeasonPrice float32 `json:"high_season_price"`
	Entry           string  `json:"entry"`
	Departure       string  `json:"departure"`
	Image           string  `gorm:"type:json" json:"image"`
	Prop            string  `gorm:"type:json" json:"prop`
}
