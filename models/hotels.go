package models

import (
	"github.com/lib/pq"
	"encoding/json"
)

// Hotel
type Hotel struct {
	ID            string `gorm:"primary_key" json:"id"`
	MembershipID  string `json:"membership_id"`
	DestinationID string `json:"destination_id"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	Address       string `json:"address"`
	Email         string `json:"email"`
	Phones        string `gorm:"type:json" json:"phones"`
	Prop          string `gorm:"type:json" json:"prop"`
}

// HotelComplex
type HotelComplex struct {
	HotelID          string             `gorm:"primary_key" json:"id"`
	DestinationID    string             `json:"destination_id"`
	MembershipID     string             `json:"membership_id"`
	HotelName        string             `json:"hotel_name"`
	Address          string             `json:"address"`
	Phones           pq.StringArray     `gorm:"type:varchar(50)[]" json:"phones"`
	Email            string             `json:"email"`
	Position         string             `json:"position"`
	HotelImages      json.RawMessage    `gorm:"type:json" json:"hotel_images"`
	HotelProps       json.RawMessage    `gorm:"type:json" json:"hotel_props"`
	HotelDesc        string             `json:"hotel_desc"`
	DestinationName  string             `json:"destination_name"`
	DestinationImage json.RawMessage    `gorm:"type:json" json:"destination_image"`
	DestinationProps json.RawMessage    `gorm:"type:json" json:"destination_props"`
	DestinationDesc  string             `json:"destination_desc"`
	Rooms            json.RawMessage    `gorm:"type:json" json:"rooms"`
	Networks         json.RawMessage    `gorm:"type:json" json:"networks"`
}
