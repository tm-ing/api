package models

type Country struct {
	ID   string `gorm:"primary_key" json:"id"`
	Name string `json:"name"`
	Code string `json:"code"`
}
