package models

type Network struct {
	ID   string `gorm:"primary_key" json:"id"`
	Name string `json:"name"`
	Icon string `json:"icon"`
}
