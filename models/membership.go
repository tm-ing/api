package models

type Membership struct {
	ID          string `gorm:"primary_key" json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
