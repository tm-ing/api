package connections

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
)

func ConnectDB() (db *gorm.DB) {

	cs := fmt.Sprint(os.Getenv("DATABASE_URL"))

	db, err := gorm.Open("postgres", cs)

	if err != nil {
		panic(err)
	}

	db.SingularTable(true)

	return db
}
