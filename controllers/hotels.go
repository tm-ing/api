package controllers

import (
"net/http"
"encoding/json"

"app/models"
instance "app/connections"
)

// GetHotels -  Get all hotels
func GetHotels(w http.ResponseWriter, req *http.Request) {

	var hotels []models.HotelComplex
	db := instance.ConnectDB()

	err := db.Raw("SELECT * FROM all_hotels").Scan(&hotels).Error

	if err != nil {
		panic(err)
	} else {
		json.NewEncoder(w).Encode(hotels)
	}

}

// GetHotel -  Get one hotel
func GetHotel(w http.ResponseWriter, req *http.Request) {

	var hotels []models.Hotel
	db := instance.ConnectDB()
	w.Header().Set("Content-Type", "application/json")

	err := db.Find(&hotels).Error

	if err != nil {
		panic(err)
	} else {
		json.NewEncoder(w).Encode(hotels)
	}

}
