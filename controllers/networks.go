package controllers

import (
	instance "app/connections"
	"app/models"
	"encoding/json"
	"net/http"
)

func GetNetworks(w http.ResponseWriter, req *http.Request) {

	var items []models.Network
	db := instance.ConnectDB()
	w.Header().Set("Content-Type", "application/json")

	err := db.Find(&items).Error

	if err != nil {
		panic(err)
	} else {
		json.NewEncoder(w).Encode(items)
	}
}
