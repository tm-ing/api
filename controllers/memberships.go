package controllers

import (
	instance "app/connections"
	"app/models"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func GetMemberships(w http.ResponseWriter, req *http.Request) {

	var items []models.Membership
	db := instance.ConnectDB()
	w.Header().Set("Content-Type", "application/json")

	err := db.Find(&items).Error

	if err != nil {
		panic(err)
	} else {
		json.NewEncoder(w).Encode(items)
	}
}

func GetMembership(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	var m models.Membership
	db := instance.ConnectDB()
	db.Where("id = ?", params["id"]).First(&m)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&m)
}

func CreateMembership(w http.ResponseWriter, req *http.Request) {
	var m models.Membership
	json.NewDecoder(req.Body).Decode(&m)
	db := instance.ConnectDB()
	db.Create(&m)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&m)
}

func DeleteMembership(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	var item models.Membership
	db := instance.ConnectDB()
	db.Where("id = ?", params["id"]).First(&item)
	db.Delete(&item)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&item)
}
