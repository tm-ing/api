package controllers

import (
	"encoding/json"
	"net/http"

	instance "app/connections"
	"app/models"
)

func GetRooms(w http.ResponseWriter, req *http.Request) {

	var items []models.Room
	db := instance.ConnectDB()
	w.Header().Set("Content-Type", "application/json")

	err := db.Find(&items).Error

	if err != nil {
		panic(err)
	} else {
		json.NewEncoder(w).Encode(items)
	}

}
