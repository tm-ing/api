package controllers

import (
	instance "app/connections"
	"app/models"
	"encoding/json"
	"net/http"
)

func GetDestinations(w http.ResponseWriter, req *http.Request) {

	var destinations []models.Destination
	db := instance.ConnectDB()
	w.Header().Set("Content-Type", "application/json")

	err := db.Find(&destinations).Error

	if err != nil {
		panic(err)
	} else {
		json.NewEncoder(w).Encode(destinations)
	}
}
