package controllers

import (
	instance "app/connections"
	"app/models"
	"encoding/json"
	"net/http"
)

func GetCountries(w http.ResponseWriter, req *http.Request) {

	var countries []models.Country
	db := instance.ConnectDB()
	w.Header().Set("Content-Type", "application/json")

	err := db.Find(&countries).Error

	if err != nil {
		panic(err)
	} else {
		json.NewEncoder(w).Encode(countries)
	}
}
