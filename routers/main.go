package routers

import (
	"fmt"
	"net/http"

	ctrl "app/controllers"

	"github.com/gorilla/mux"
)

func RestMiddleWare(next http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		next.ServeHTTP(w, r)

	}

}

func mainHandler(r *mux.Router) {

	r.HandleFunc("/", Hello).Methods("GET")
	r.HandleFunc("/hotels", RestMiddleWare(ctrl.GetHotels)).Methods("GET")

	r.HandleFunc("/rooms", ctrl.GetRooms).Methods("GET")
	r.HandleFunc("/countries", ctrl.GetCountries).Methods("GET")
	r.HandleFunc("/destinations", RestMiddleWare(ctrl.GetDestinations)).Methods("GET")
	r.HandleFunc("/networks", ctrl.GetNetworks).Methods("GET")

	r.HandleFunc("/memberships", ctrl.GetMemberships).Methods("GET")
	r.HandleFunc("/memberships/{id}", ctrl.GetMembership).Methods("GET")
	r.HandleFunc("/memberships", ctrl.CreateMembership).Methods("POST")
	r.HandleFunc("/memberships/{id}", ctrl.DeleteMembership).Methods("DELETE")
}

func NewRouter() *mux.Router {

	r := mux.NewRouter()
	mainHandler(r)

	return r

}

func Hello(w http.ResponseWriter, req *http.Request) {

	fmt.Fprintln(w, "Hotellium API 1.0!")

}
