package main

import (
	"fmt"
	"log"
	"net/http"

	"app/routers"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

/*func withLogging(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Logged connection from %s", r.RemoteAddr)
		next.ServeHTTP(w, r)
	}
}*/

func main() {

	router := routers.NewRouter()
	http.Handle("/", router)
	fmt.Println("Starting up on 3500")
	log.Fatal(http.ListenAndServe(":3500", nil))

}
